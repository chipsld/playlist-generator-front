import { createRouter, createWebHistory } from 'vue-router'
const LoginPage = () => import("@/pages/login-page.vue");
const MainPage = () => import("@/pages/main-page.vue");
const PlaylistPage = () => import("@/pages/playlist-page.vue");

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: LoginPage,
      beforeEnter : (to, from , next) => {
        if (localStorage.getItem('user')) {
          return next({
            name: 'main'
          })
        }
        next()
      }
    },
    {
      path: '/main',
      name: 'main',
      component: MainPage,
      beforeEnter : (to, from , next) => {
        if (!localStorage.getItem('user')) {
          return next({
            name: 'home'
          })
        }
        next()
      }
    },
    {
      path: '/playlist',
      name: 'playlist',
      component: PlaylistPage,
    }
  ]
})



export default router
